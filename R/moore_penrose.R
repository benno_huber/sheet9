#' Invert numeric vectors
#'
#' @param x A numeric vector.
#'
#' @return A numeric vector containing the inverse of x element by element, 0 if
#'   the element was 0.
#'
#' @examples
#' lambda_plus(c(1, 4, 2))
#' lambda_plus(c(1, 0, 4, 0))
lambda_plus <- function(x){
  if (any(is.na(x))) return(NA)
  nonzero <- x != 0
  x[nonzero] <- 1 / x[nonzero]
  x
}


#' Calculate Moore-Penrose inverse
#'
#' @param A A symmetric, positiv-semidefinite square matrix.
#'
#' @return The Moore-Penrose inverse of A.
#'
#' @examples
#' A <- matrix(c(1, 0, 0, 2), ncol = 2)
#' moore_penrose(A)
#' 
#' A %*% moore_penrose(A) %*% A
moore_penrose <- function(A){
  eig_res <- eigen(A)
  eig_res$vectors %*% diag(lambda_plus(eig_res$values)) %*% t(eig_res$vectors)
}
